/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprog;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import util.ManejoArchivos;
import util.Util;

/**
 *
 * @author nati2
 */
public class Logica {

    private ManejoArchivos ma;

    private static final String RUTA_USUAR = "datos/usuarios.txt";
    private static final String RUTA_CONTACT = "datos/contactos.txt";
    private static final String RUTA_ACTI = "datos/actividades.txt";

    /**
     * Llama al manejo de ar
     */
    public Logica() {
        ma = new ManejoArchivos();
    }

    /**
     * Registra un contacto nuevo
     *
     * @param nuevo Es el contacto nuevo a registrar
     */
    public void registrarContacto(Contacto nuevo) {
        ma.escribir(RUTA_CONTACT, nuevo.getInfoCont());
    }

    /**
     * Registra un usuario
     *
     * @param nueva es la nueva persona a registrar
     */
    public void registrarUsuario(Persona nueva) {
        ma.escribir(RUTA_USUAR, nueva.getInfo());
    }

    /**
     * Registra una nueva actividad
     *
     * @param nueva es la actividad a registrar
     */
    public void registrarActividad(Actividad nueva) {
        ma.escribir(RUTA_ACTI, nueva.getInfoActi());
    }

    /**
     * Verifica que los datos de usuario sean correctos para iniciar sesión
     *
     * @param usuario nombre de usuario de la persona
     * @param contr contraseña elegida por la persona
     * @return true si los datos coinciden, false si no.
     */
    public boolean iniciarS(String usuario, String contr) {
        boolean resp = false;
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        for (String i : personas) {
            String[] datos = i.split(",");
            if (usuario.equalsIgnoreCase(datos[5]) && contr.equals(datos[4])) {
                resp = true;
            }
        }
        return resp;
    }

//    public boolean validarDatos(String ced, Date naci, String correo) {
//        boolean resp = false;
//        String[] personas = ma.leer(RUTA_USUAR).split("\n");
//
//        for (String i : personas) {
//            String[] datos = i.split(",");
//            if (ced.equals(datos[2]) && naci.equals(datos[3]) && correo.equals(datos[1])) {
//                resp = true;
//            }
//
//        }
//        return resp;
//    }
//
//
//        
//    
    public String cont(String cedula, Date fecha, String correo) {
        String resp = "";
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        System.out.println("ENTRÉ");
        for (String i : personas) {
            String[] datos = i.split(",");
            if (cedula.equals(datos[3]) && fecha.equals(datos[4]) && correo.equals(datos[2])) {
                System.out.println(datos[2]);
                resp = "Sirve";
            }
        }
        return resp;
    }
//    public int[] nuevaCont(boolean validar) {
//
//        if (validar == true) {
//            int[] contra = new int[10];
//            for (int x : contra) {
//                contra[x] = (int) (Math.random() * 10) + 1;
//            }
//
//        }
//        return contra;
//    }

    /**
     * Resive los datos digitados por el usuario
     *
     * @return Retorna el arreglo con los datos del usuario
     */
    public Persona[] consultarPersona() {
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        Persona[] arr = new Persona[personas.length];

        for (int i = 0; i < personas.length; i++) {
            String[] datos = personas[i].split(",");

            Persona per = new Persona();
            per.setNombre(datos[0]);
            per.setCorreo(datos[1]);
            per.setCedula(datos[2]);
            per.setFechaNaci(Util.convertirFecha(datos[3]));
            per.setContraseña(datos[4]);
            per.setUsuario(datos[5]);
            per.setNumTelef(Integer.parseInt(datos[6]));

            arr[i] = per;
        }
        return arr;
    }

    /*
    Consulta 
    
     */
    public Contacto[] consultarContact() {
        String[] contactos = ma.leer(RUTA_CONTACT).split("\n");
        Contacto[] arr = new Contacto[contactos.length];

        for (int i = 0; i < contactos.length; i++) {
            String[] datos = contactos[i].split(",");

            Contacto con = new Contacto();
            con.setNombre(datos[0]);
            con.setFechaNaci(Util.convertirFecha(datos[1]));
            con.setCorreo(datos[2]);
            con.setNumTelef(Integer.parseInt(datos[3]));
            con.setDescr(datos[4]);
            arr[i] = con;
        }
        return arr;
    }

    public Contacto[] consultarContact(String correo) {
        String[] contactos = ma.leer(RUTA_CONTACT).split("\n");
        Contacto[] arr = new Contacto[contactos.length];
        int i = 0;

        for (String linea : contactos) {

            String[] datos = linea.split(",");
            if (correo.equals(datos[2])) {

                Contacto con = new Contacto();
                con.setNombre(datos[0]);
                con.setFechaNaci(Util.convertirFecha(datos[1]));
                con.setCorreo(datos[2]);
                con.setNumTelef(Integer.parseInt(datos[3]));
                con.setDescr(datos[4]);
                arr[i] = con;
            }
        }
        return arr;
    }

    public Actividad[] consultarActividad() {
        String[] actividades = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arrAct = new Actividad[actividades.length];

        for (int i = 0; i < actividades.length; i++) {
            String[] datos = actividades[i].split(",");

            Actividad act = new Actividad();
            act.setNomAct(datos[0]);
            act.setFecha(Util.convertirFecha(datos[1]));
            act.setHora(datos[2]);
            act.setFestivo(Boolean.parseBoolean(datos[3]));
            act.setContacto(datos[4]);
            arrAct[i] = act;

        }
        return arrAct;
    }

    public Actividad[] consultarActividad(String fecha) {
        String[] actividades = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arrAct = new Actividad[actividades.length];
        int i = 0;

        for (String linea : actividades) {

            String[] datos = linea.split(",");
            if (fecha.equals(datos[1])) {

                Actividad act = new Actividad();
                act.setNomAct(datos[0]);
                act.setFecha(Util.convertirFecha(datos[1]));
                act.setHora(datos[2]);
                act.setFestivo(Boolean.parseBoolean(datos[3]));
                act.setContacto(datos[4]);
                arrAct[i] = act;
            }
        }
        return arrAct;
    }

    public String impCont(Contacto[] contactos) {
        String msj = "";
        String formato = "%d. %s: %s, %s, %d, %d, %s\n";

        for (int i = 0; i < contactos.length; i++) {
            Contacto x = contactos[i];
            if (x == null) {
                return msj;
            }
            int edad = calcEdad(x.getFechaNaciString());

            msj += String.format(formato, (i + 1),
                    x.getNombre(), x.getFechaNaciString(), x.getCorreo(), x.getNumTelef(),
                    edad, x.getDescr());
        }
        return msj;
    }

    public String impAct(Actividad[] actividades) {
        String msj = "";
        String formato = "%d. %s: %s, %s, %d, %d, %s\n";

        for (int i = 0; i < actividades.length; i++) {
            Actividad a = actividades[i];
            if (a == null) {
                return msj;
            }

            String fes = a.isFestivo() ? "Festivo" : "No es festivo";

            msj += String.format(formato, (i + 1),
                    a.getNomAct(), a.getFechaString(), a.getHora(), fes,
                    a.getContacto());
        }
        return msj;
    }

    /**
     * Calcula la edad de la persona
     *
     * @param fecha es la fecha de nacimiento de la persona.
     * @return edad de la persona
     */
    private int calcEdad(String fecha) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecNan = LocalDate.parse(fecha, dtf);
        LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(fecNan, hoy);
        return periodo.getYears();
    }

    /*
    Sirve para editar cualquier dato del contacto.
     */
    public void editarContact(String info, Contacto temp) {
        String datos = ma.leer(RUTA_CONTACT);
        datos = datos.replaceAll(info, temp.getInfoCont()).trim();
        ma.escribir(RUTA_CONTACT, datos, false);
    }

    /*
    Elimina el contacto que desea el usuario.
     */
    public void eliminarContac(Contacto contacto) {
        String datos = ma.leer(RUTA_CONTACT);
        datos = datos.replaceAll(contacto.getInfoCont() + "\n", "").trim();
        ma.escribir(RUTA_CONTACT, datos, false);
    }

    /*
    Elimina la actividad que desea el usuario.
     */
    public void eliminarAct(Actividad actividad) {
        String datos = ma.leer(RUTA_ACTI);
        datos = datos.replaceAll(actividad.getInfoActi() + "\n", "").trim();
        ma.escribir(RUTA_ACTI, datos, false);
    }

    public String hora(int horario, String hora) {
        if (horario == 1) {
            hora += " AM";
        } else {
            hora += " PM";
        }
        return hora;
    }

}

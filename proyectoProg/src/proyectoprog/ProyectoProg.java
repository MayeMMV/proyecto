/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprog;

import java.util.Date;
import util.Util;

/**
 *
 * @author Mayela Mejía Vela
 */
public class ProyectoProg {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Logica log = new Logica();
        

        String menuP = "Menú Principal\n"
                + "1. Iniciar Sesión\n"
                + "2. Registrarse\n"
                + "3. Olvidé la Contraseña\n"
                + "4. Salir";

        String menuIni = "Bienvenido.\n"
                + "1. Actividades\n"
                + "2. Contactos\n"
                + "3. Cerrar Sesión";

        String menuAct = "Actividades\n"
                + "1. Registrar\n"
                + "2. Editar\n"
                + "3. Eliminar\n"
                + "4. Volver";
        String menuCont = "Contactos\n"
                + "1. Registrar\n"
                + "2. Editar\n"
                + "3. Eliminar\n"
                + "4. Volver";

        String salir = "¿Está seguro que desea salir?";
        String cerrarS = "¿Está seguro que desea cerrar sesión?";

        SALIR:
        while (true) {
            int op = Util.leerInt(menuP);
            switch (op) {
                case 1:
                    String nomUsuario = Util.leerTexto("Ingrese su nombre de usuario");
                    String contras = Util.leerTexto("Ingrese su contraseña");
                    if (log.iniciarS(nomUsuario, contras) == true) {

                        SESION:
                        while (true) {
                            op = Util.leerInt(menuIni);
                            switch (op) {
                                case 1:
                                    ACT:
                                    while (true) {
                                        op = Util.leerInt(menuAct);
                                        switch (op) {
                                            case 1:
                                                String nombre = Util.leerTexto("Nombre de actividad");
                                                Date fecha = Util.leerFecha("Fecha de actividad");
                                                int horario = Util.leerInt("Seleccione un horario\n"
                                                        + "1. Mañana\n"
                                                        + "2.Tarde");
                                                
                                                String hor = Util.leerTexto("Digite la hora de la actividad (00:00)");
                                                String ho = log.hora(horario, hor);
                                                boolean festivo = Util.confirmar("¿Es día festivo?");
                                                boolean confContacto = Util.confirmar("¿Desea vincular algún contacto?");
                                                String contacto = null;
                                                if (confContacto == true){
                                                    contacto = Util.leerTexto("Correo de contacto");
                                                }
                                                
                                                Actividad act = new Actividad(nombre,fecha, ho, festivo, contacto);
                                                log.registrarActividad(act);
                                                Util.mostrar("Actividad registrada exitosamente.");
                    
                                                break;
                                            case 2:
                                                break;
//                                            case 3:
//                                                fecha = Util.leerFecha("Digite la fecha de la actividad, "
//                                                        + "o equis (X) para mostrar las actividades.");
//                                                
//                                                Actividad [] elAct = "x".equals(fecha) ? log.consultarActividad() : Util.leerFecha(log.consultarActividad(fecha));
//                                                
//                                                int num = Util.leerInt("Actividades:\n"
//                                                        + log.impAct(elAct)
//                                                        + "\nSeleccione la actividad");
//
//                                                Actividad elimAct = elAct[num - 1];
//                                                String info = elimAct.getInfoActi();
//                                                nombre = Util.leerTexto("Nombre de actividad", elimAct.getNomAct());
//                                                fecha = Util.leerFecha("Fecha de la actividad", elimAct.getFechaString());
//                                                String hora = Util.leerTexto("Hora", elimAct.getHora());
//                                                festivo = Util.confirmar("¿Es día festivo?");
//                                                confContacto = Util.confirmar("¿Desea vincular algún contacto?");
//
//                                                act = new Actividad(nombre, fecha, hora, festivo, confContacto);
//                                                log.editarContact(info, cont);
//                                                ediCon = log.consultarContact(cor);
//                                                Util.mostrar(log.impCont(ediCon));
                                                //break;
                                            case 4:
                                                break ACT;
                                            default:
                                                Util.mostrar("Opción inválida, seleccione una opción correcta.");
                                        }
                                    }
                                break;    
                                case 2:
                                    VOLVER:
                                    while (true) {
                                        op = Util.leerInt(menuCont);
                                        switch (op) {
                                            case 1:
                                                String nombre = Util.leerTexto("Nombre");
                                                Date fecha = Util.leerFecha("Fecha de Nacimiento");
                                                String correo = Util.leerTexto("Correo Electrónico");
                                                int telef = Util.leerInt("Número de teléfono");
                                                String desc = Util.leerTexto("Descripción");

                                                Contacto cont = new Contacto(nombre, fecha, correo, telef, desc);

                                                log.registrarContacto(cont);
                                                Util.mostrar("Contacto registrado exitosamente.");
                                                break;
                                            case 2:
                                                String cor = Util.leerTexto("Digite el correo electrónico"
                                                        + " del contacto, o equis (X) para mostrar los contactos.");
                                                
                                                Contacto [] ediCon = "x".equalsIgnoreCase(cor) ? log.consultarContact() : log.consultarContact(cor);
                                                
                                                int num = Util.leerInt("Contactos:\n"
                                                        + log.impCont(ediCon)
                                                        + "\nSeleccione el Contacto");

                                                Contacto meEdiCo = ediCon[num - 1];
                                                String info = meEdiCo.getInfoCont();
                                                nombre = Util.leerTexto("Nombre", meEdiCo.getNombre());
                                                fecha = Util.leerFecha("Fecha Nacimiento", meEdiCo.getFechaNaciString());
                                                correo = Util.leerTexto("Correo", meEdiCo.getCorreo());
                                                telef = Util.leerInt("Teléfono", meEdiCo.getNumTelef());
                                                desc = Util.leerTexto("Descripcion", meEdiCo.getDescr());

                                                cont = new Contacto(nombre, fecha, correo, telef, desc);
                                                log.editarContact(info, cont);
                                                ediCon = log.consultarContact(cor);
                                                Util.mostrar(log.impCont(ediCon));
                                                break;
                                            case 3:
                                                cor = Util.leerTexto("Digite el correo del contacto, o digite equis (X), para"
                                                        + "ver los contactos.");
                                                ediCon = "x".equalsIgnoreCase(cor) ? log.consultarContact() : log.consultarContact(cor);
                                                num = Util.leerInt("Contactos:\n"
                                                        + log.impCont(ediCon)
                                                        + "\nSeleccione un contacto");
                                                Contacto conEli = ediCon[num - 1];
                                                if (Util.confirmar("¿Está seguro que desea eliminar a: " + conEli.getNombre())) {
                                                    log.eliminarContac(conEli);
                                                }
                                                break;
                                            case 4:
                                                break VOLVER;
                                            default:
                                                Util.mostrar("Opción inválida, seleccione una opción correcta.");
                                        } 
                                        
                                    }
                                    break;
                                case 3:
                                    if (Util.confirmar(cerrarS)) {
                                        break SESION;
                                    } else {
                                        break;
                                    }
                                default:
                                    Util.mostrar("Opción inválida, seleccione una opción correcta.");
                            }
                        }
                    } else {
                        Util.mostrar("Usuario o contraseña inválida.");
                    }

                    break;
                case 2:
                    String nombre = Util.leerTexto("Nombre");
                    String correo = Util.leerTexto("Correo Electrónico");
                    String cedula = Util.leerTexto("Cédula");
                    Date fecha = Util.leerFecha("Fecha de Nacimiento");
                    contras = Util.leerTexto("Contraseña");
                    nomUsuario = Util.leerTexto("Usuario");
                    int telef = Util.leerInt("Número de teléfono");

                    Persona pers = new Persona(nombre, correo, cedula, fecha, contras, nomUsuario, telef);

                    log.registrarUsuario(pers);
                    Util.mostrar("Usuario registrado exitosamente.");
                    break;
                case 3:
                    cedula = Util.leerTexto("¿Cúal es su número de cédula?");
                    fecha = Util.leerFecha("¿Cúal es su fecha de nacimiento");
                    correo = Util.leerTexto("¿Cúal es su correo electrónico?");
                    String sa = log.cont(cedula, fecha, correo);
                    System.out.println(sa);
                    //boolean vali = log.validarDatos(cedula, fecha, correo);
                    //if (vali == true){
                        
                    //}

                    System.out.println("");
                    break;
                case 4:
                    if (Util.confirmar(salir)) {
                        break SALIR;
                    } else {
                        break;
                    }
                default:
                    Util.mostrar("Opción inválida, seleccione una opción correcta.");
            }
        }

    }

}

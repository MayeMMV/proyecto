/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprog;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nati2
 */
public class Actividad {
    
    private String nomAct;
    private Date fecha;
    private String hora;
    private boolean festivo;
    private String contacto;

    public Actividad(String nomAct, Date fecha, String hora, boolean festivo, String contacto) {
        this.nomAct = nomAct;
        this.fecha = fecha;
        this.hora = hora;
        this.festivo = festivo;
        this.contacto = contacto;
    }
    
    public Actividad(){
    }

    public String getNomAct() {
        return nomAct;
    }

    public void setNomAct(String nomAct) {
        this.nomAct = nomAct;
    }

    public Date getFecha() {
        return fecha;
    }
    
    public String getFechaString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fecha);
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public boolean isFestivo() {
        return festivo;
    }

    public void setFestivo(boolean festivo) {
        this.festivo = festivo;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }
    
    public String getInfoActi() {
        String temp = "%s,%s,%s,%s,%s";
        return String.format(temp, nomAct, getFechaString(), hora, festivo, contacto);
    }

    @Override
    public String toString() {
        return "Actividad{" + "nomAct=" + nomAct + ", fecha=" + fecha + ", hora=" + hora + ", festivo=" + festivo + ", contacto=" + contacto + '}';
    }
    
    
    
}

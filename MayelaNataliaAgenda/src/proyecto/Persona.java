/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Mayela Mejía Vela
 */
public class Persona {
    
    //Atributos
    private String nombre;
    private String correo;
    private String cedula;
    private Date fechaNaci;
    private String contraseña;
    private String usuario;
    private int numTelef;
    
    
    //Constructores

    public Persona(String nombre, String correo, String cedula, Date fechaNaci, String contraseña, String usuario, int numTelef) {
        this.nombre = nombre;
        this.correo = correo;
        this.cedula = cedula;
        this.fechaNaci = fechaNaci;
        this.contraseña = contraseña;
        this.usuario = usuario;
        this.numTelef = numTelef;
    }
    
    public Persona(){
        
    }
    
    //Get & set

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Date getFechaNaci() {
        return fechaNaci;
    }

    public String getFechaNaciString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNaci);
    }
    
    public void setFechaNaci(Date fechaNaci) {
        this.fechaNaci = fechaNaci;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getNumTelef() {
        return numTelef;
    }

    public void setNumTelef(int numTelef) {
        this.numTelef = numTelef;
    }
    
    public String getInfo() {
        String temp = "%s,%s,%s,%s,%s,%s,%d";
        return String.format(temp, nombre, correo, cedula, getFechaNaciString(), contraseña, usuario, numTelef);
    }
    
    //ToString
    @Override
    public String toString() {
        return "registroPersona{" + "nombre=" + nombre + ", correo=" + correo + ", cedula=" + cedula + ", fechaNaci=" + fechaNaci + ", contrase\u00f1a=" + contraseña + ", usuario=" + usuario + ", numTelef=" + numTelef + '}';
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import util.ManejoArchivos;
import util.Util;

/**
 *
 * @author nati2
 */
public class Logica {

    private ManejoArchivos ma;

    private static final String RUTA_USUAR = "datos/usuarios.txt";
    private static final String RUTA_CONTACT = "datos/contactos.txt";
    private static final String RUTA_ACTI = "datos/actividades.txt";

    /**
     * Llama al manejo de ar
     */
    public Logica() {
        ma = new ManejoArchivos();
    }

    /**
     * Registra un contacto nuevo
     *
     * @param nuevo Es el contacto nuevo a registrar
     */
    public void registrarContacto(Contacto nuevo) {
        ma.escribir(RUTA_CONTACT, nuevo.getInfoCont());
    }

    /**
     * Registra un usuario
     *
     * @param nueva es la nueva persona a registrar
     */
    public void registrarUsuario(Persona nueva) {
        ma.escribir(RUTA_USUAR, nueva.getInfo());
    }

    /**
     * Registra una nueva actividad
     *
     * @param nueva es la actividad a registrar
     */
    public void registrarActividad(Actividad nueva) {
        ma.escribir(RUTA_ACTI, nueva.getInfoActi());
    }

    /**
     * Verifica que los datos de usuario sean correctos para iniciar sesión
     *
     * @param usuario nombre de usuario de la persona
     * @param contr contraseña elegida por la persona
     * @return true si los datos coinciden, false si no.
     */
    public boolean iniciarS(String usuario, String contr) {
        boolean resp = false;
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        for (String i : personas) {
            String[] datos = i.split(",");
            if (usuario.equalsIgnoreCase(datos[5]) && contr.equals(datos[4])) {
                resp = true;
            }
        }
        return resp;
    }
    
    public String newCont(String cedula, int telefono, String correo) {
        String resp = "Respuestas incorrectas. Intente de nuevo.";
        int[] con = new int[10];
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        String x = "" + telefono;

        for (String i : personas) {
            String[] datos = i.split(",");
            if (cedula.equals(datos[2]) && x.equals(datos[6]) && correo.equals(datos[1])) {
                for (int num = 0; num < con.length; num++) {
                    con[num] = (int) (Math.random() * 10) + 1;
                }
                String conS = Arrays.toString(con);
//                datos.set(4, conS);
//                datos[4] = conS;
//                System.out.println(datos[4]);
//                correo = Util.leerTexto("Correo", datos.getContraseña());
//                datos = datos.replaceAll(datos[4],conS);
//                ma.escribir(RUTA_USUAR, datos, false);

                resp = "Su nueva contraseña es: " + conS;
                
            }
        }
        return resp;
    }
    
    public void editarContrase(String nContr, Persona temp) {
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        Persona[] arr = new Persona[personas.length];
        
        //personas = personas.replaceAll(arr[4], nContr).trim();
        //ma.escribir(RUTA_USUAR, personas, false);
    }
    
/**
 * Guarda los datos de la persona en un archivo.
 * @return un arreglo con todos los datos de la nueva persona.
 */
    public Persona[] consultarPersona() {
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        Persona[] arr = new Persona[personas.length];

        for (int i = 0; i < personas.length; i++) {
            String[] datos = personas[i].split(",");

            Persona per = new Persona();
            per.setNombre(datos[0]);
            per.setCorreo(datos[1]);
            per.setCedula(datos[2]);
            per.setFechaNaci(Util.convertirFecha(datos[3]));
            per.setContraseña(datos[4]);
            per.setUsuario(datos[5]);
            per.setNumTelef(Integer.parseInt(datos[6]));

            arr[i] = per;
        }
        return arr;
    }
/**
 * Sirve para modificar los datos del usuario
 * @param contraseña contraseña de la persona
 * @return devuelve los nuevos datos editados por el usuario
 */
    public Persona[] consultarPersona(String contraseña) {
        String[] personas = ma.leer(RUTA_USUAR).split("\n");
        Persona[] arr = new Persona[personas.length];
        int i = 0;

        for (String linea : personas) {
            String[] datos = linea.split(",");
            if (contraseña.equals(datos[4])) {
                Persona per = new Persona();
                per.setNombre(datos[0]);
                per.setCorreo(datos[1]);
                per.setCedula(datos[2]);
                per.setFechaNaci(Util.convertirFecha(datos[3]));
                per.setContraseña(datos[4]);
                per.setUsuario(datos[5]);
                per.setNumTelef(Integer.parseInt(datos[6]));
                arr[i] = per;
            }
        }
        return arr;
    }
/**
 * Guarda los datos del contacto en un archivo.
 * @return un arreglo con todos los datos del nuevo contacto.
 */
    public Contacto[] consultarContact() {
        String[] contactos = ma.leer(RUTA_CONTACT).split("\n");
        Contacto[] arr = new Contacto[contactos.length];

        for (int i = 0; i < contactos.length; i++) {
            String[] datos = contactos[i].split(",");

            Contacto con = new Contacto();
            con.setNombre(datos[0]);
            con.setFechaNaci(Util.convertirFecha(datos[1]));
            con.setCorreo(datos[2]);
            con.setNumTelef(Integer.parseInt(datos[3]));
            con.setDescr(datos[4]);
            arr[i] = con;
        }
        return arr;
    }
/**
 * El usuario puede modificar los datos del contacto
 * @param correo correo del usuario que se va a modificar
 * @return devuelve los datos editados del contacto
 */
    public Contacto[] consultarContact(String correo) {
        String[] contactos = ma.leer(RUTA_CONTACT).split("\n");
        Contacto[] arr = new Contacto[contactos.length];
        int i = 0;

        for (String linea : contactos) {

            String[] datos = linea.split(",");
            if (correo.equals(datos[2])) {

                Contacto con = new Contacto();
                con.setNombre(datos[0]);
                con.setFechaNaci(Util.convertirFecha(datos[1]));
                con.setCorreo(datos[2]);
                con.setNumTelef(Integer.parseInt(datos[3]));
                con.setDescr(datos[4]);
                arr[i] = con;
            }
        }
        return arr;
    }
/**
 * Sirve para editar un contacto 
 * @param conf contacto que desea vincular
 * @return los datos digitados
 */
    public Actividad[] consultarActividad(boolean conf) {
        String[] actividades = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arrAct = new Actividad[actividades.length];

        for (int i = 0; i < actividades.length; i++) {
            String[] datos = actividades[i].split(",");

            Actividad act = new Actividad();
            act.setNomAct(datos[0]);
            act.setFecha(Util.convertirFecha(datos[1]));
            act.setHora(datos[2]);
            act.setFestivo(Boolean.parseBoolean(datos[3]));
            if (conf == true) {
                act.setContacto(datos[4]);
            }
            arrAct[i] = act;

        }
        return arrAct;
    }
/**
 * Guarda la actividad del usuario en un archivo
 * @return el archivo con los datos digitados por el usuario
 */
    public Actividad[] consultarActividad() {
        String[] actividades = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arrAct = new Actividad[actividades.length];

        for (int i = 0; i < actividades.length; i++) {
            String[] datos = actividades[i].split(",");

            Actividad act = new Actividad();
            act.setNomAct(datos[0]);
            act.setFecha(Util.convertirFecha(datos[1]));
            act.setHora(datos[2]);
            act.setFestivo(Boolean.parseBoolean(datos[3]));
            act.setContacto(datos[4]);
            arrAct[i] = act;

        }
        return arrAct;
    }
/**
 * Sirve para editar una actividad
 * @param nombre nombre de la actividad
 * @param conf contacto que desea vincular
 * @return Retorna los nuevos datos digitados
 */
    public Actividad[] consultarActividad(String nombre, boolean conf) {
        String[] actividades = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arrAct = new Actividad[actividades.length];

        for (int i = 0; i < actividades.length; i++) {
            String[] datos = actividades[i].split(",");

            if (nombre.equals(datos[0])) {

                Actividad act = new Actividad();
                act.setNomAct(datos[0]);
                act.setFecha(Util.convertirFecha(datos[1]));
                act.setHora(datos[2]);
                act.setFestivo(Boolean.parseBoolean(datos[3]));
                if (conf == true) {
                    act.setContacto(datos[4]);
                }
                arrAct[i] = act;
            }
        }
        return arrAct;
    }
/**
 * Muestra todos los usuarios registrados
 * @param usuarios usuarios registrados
 * @return Retorna el formato con todos los usuarios
 */
    public String impPers(Persona[] usuarios) {
        String msj = "";
        String formato = "1. %s, %s, %s, %s, %s, %s, %d\n";

        for (int i = 0; i < usuarios.length; i++) {
            Persona p = usuarios[i];
            if (p == null) {
                return msj;
            }

            msj += String.format(formato,
                    p.getNombre(), p.getCorreo(), p.getCedula(), p.getFechaNaciString(),
                    p.getContraseña(), p.getUsuario(), p.getNumTelef());
        }
        return msj;
    }
/**
 * Muestra los contactos
 * @param contactos contactos del usuario
 * @return los contactos del usuario
 */
    public String impCont(Contacto[] contactos) {
        String msj = "";
        String formato = "%d. %s: %s, %s, %d, %d, %s\n";

        for (int i = 0; i < contactos.length; i++) {
            Contacto x = contactos[i];
            if (x == null) {
                return msj;
            }
            int edad = calcEdad(x.getFechaNaciString());

            msj += String.format(formato, (i + 1),
                    x.getNombre(), x.getFechaNaciString(), x.getCorreo(), x.getNumTelef(),
                    edad, x.getDescr());
        }
        return msj;
    }
/**
 * Muestra todas las actividades
 * @param actividades actividades del usuario
 * @return Formato con todos los datos
 */
    public String impAct(Actividad[] actividades) {
        String msj = "";
        String formato = "%d. %s: %s, %s, %s, %s, %s\n";

        for (int i = 0; i < actividades.length; i++) {
            Actividad a = actividades[i];
            if (a == null) {
                return msj;
            }

            String fes = a.isFestivo() ? "Festivo" : "No es festivo";
            String cont = a.isConfContacto() ? "Contacto vinculado" : "Sin contacto vinculado";

            msj += String.format(formato, (i + 1),
                    a.getNomAct(), a.getFechaString(), a.getHora(), fes, cont,
                    a.getContacto());
        }
        return msj;
    }

    /**
     * Calcula la edad de la persona
     *
     * @param fecha es la fecha de nacimiento de la persona.
     * @return edad de la persona
     */
    private int calcEdad(String fecha) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecNac = LocalDate.parse(fecha, dtf);
        LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(fecNac, hoy);
        return periodo.getYears();
    }
/**
 * Edita el usuario que desea.
 * @param info Informacion actual del usuario. 
 * @param temp Informacion nueva del usuario.
 */
    public void editarUsuario(String info, Persona temp) {
        String datos = ma.leer(RUTA_USUAR);
        datos = datos.replaceAll(info, temp.getInfo()).trim();
        ma.escribir(RUTA_USUAR, datos, false);
    }
/**
 * Edita el contacto que el usuario desea.
 * @param info Informacion actual del contacto.
 * @param temp Informacion nueva del usuario.
 */
    public void editarContact(String info, Contacto temp) {
        String datos = ma.leer(RUTA_CONTACT);
        datos = datos.replaceAll(info, temp.getInfoCont()).trim();
        ma.escribir(RUTA_CONTACT, datos, false);
    }
/**
 * Edita la actividad que el usuario desea.
 * @param info Informacion actual de la actividad.
 * @param act Informacion nueva de la actividad.
 */
    public void editarActi(String info, Actividad act) {
        String datos = ma.leer(RUTA_ACTI);
        datos = datos.replaceAll(info, act.getInfoActi()).trim();
        ma.escribir(RUTA_ACTI, datos, false);
    }
/**
 * Elimina el contacto que desea el usuario
 * @param contacto Contacto que se va a eliminar
 */
    public void eliminarContac(Contacto contacto) {
        String datos = ma.leer(RUTA_CONTACT);
        datos = datos.replaceAll(contacto.getInfoCont() + "\n", "").trim();
        ma.escribir(RUTA_CONTACT, datos, false);
    }
/**
 * Elimina la actividad que desea el usuario.
 * @param actividad Actividad que se va a eliminar
 */
    public void eliminarAct(Actividad actividad) {
        String datos = ma.leer(RUTA_ACTI);
        datos = datos.replaceAll(actividad.getInfoActi() + "\n", "").trim();
        ma.escribir(RUTA_ACTI, datos, false);
    }
/**
 * Permite añadirle el am o pm a la hora de la actividad
 * @param horario turno en el que desea (mañana o tarde).
 * @param hora La hora en que va a ser la actividad.
 * @return La hora con el am o pm.
 */
    public String hora(int horario, String hora) {
        if (horario == 1) {
            hora += " AM";
        } else {
            hora += " PM";
        }
        return hora;
    }

}

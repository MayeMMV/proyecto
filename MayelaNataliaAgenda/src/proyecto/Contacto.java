/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nati2
 */
public class Contacto {

    //Atributos
    private String nombre;
    private Date fechaNaci;
    private String correo;
    private int numTelef;
    private String descr;

    //Constructores
    public Contacto(String nombre, Date fechaNaci, String correo, int numTelef, String descr) {
        this.nombre = nombre;
        this.fechaNaci = fechaNaci;
        this.correo = correo;
        this.numTelef = numTelef;
        this.descr = descr;
    }

    Contacto() {
        
    }

    //Get && Set
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNaci() {
        return fechaNaci;
    }

    public String getFechaNaciString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNaci);
    }

    public void setFechaNaci(Date fechaNaci) {
        this.fechaNaci = fechaNaci;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getNumTelef() {
        return numTelef;
    }

    public void setNumTelef(int numTelef) {
        this.numTelef = numTelef;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getInfoCont() {
        String temp = "%s,%s,%s,%s,%s";
        return String.format(temp, nombre, getFechaNaciString(), correo, numTelef, descr);
    }

    @Override
    public String toString() {
        return "Contacto{" + "nombre=" + nombre + ", fechaNaci=" + fechaNaci + ", correo=" + correo + ", numTelef=" + numTelef + ", descr=" + descr + '}';
    }

}

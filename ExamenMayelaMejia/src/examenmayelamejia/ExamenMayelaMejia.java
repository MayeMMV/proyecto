/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenmayelamejia;

import javax.swing.JOptionPane;

/**
 *
 * @author Mayela Mejía Vela
 */
public class ExamenMayelaMejia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //N1
        Logica log = new Logica();
        int n1 = Integer.parseInt(JOptionPane.showInputDialog("Digite el primer número: "));
        int n2 = Integer.parseInt(JOptionPane.showInputDialog("Digite el segundo número: "));
        String r = log.numero(n1, n2);
        System.out.println(r);

        //N2
//        int n3 = Integer.parseInt(JOptionPane.showInputDialog("Digite el numero tamaño del arreglo: "));
//        int n4 = Integer.parseInt(JOptionPane.showInputDialog("Digite la dimensión del arreglo: "));
//        int[] arreglo = log.arreglo(n3);

        //N3
        int pri = Integer.parseInt(JOptionPane.showInputDialog("Digite el numero: "));
        String res = log.primo(pri);
        System.out.println(res);

        String menu = "**Menu Opciones**\n"
                + "1. Imprimir números\n"
                + "2. Generar arreglo\n"
                + "3. Numero primo\n"
                + "4. Salir";
        OUT:
        while (true) {
            int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
            switch (op) {
                case 1:
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("Digite el primer número: "));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("Digite el segundo número: "));
                    r = log.numero(n1, n2);
                    System.out.println(r);
                    break;
                case 2:
//                    n3 = Integer.parseInt(JOptionPane.showInputDialog("Digite el numero tamaño del arreglo: "));
//                    n4 = Integer.parseInt(JOptionPane.showInputDialog("Digite la dimensión del arreglo: "));
//                  arreglo = log.arreglo(n3, n4);
                    System.out.println("arreglo");
                    break;
                case 3:
                    pri = Integer.parseInt(JOptionPane.showInputDialog("Digite el numero: "));
                    res = log.primo(pri);
                    System.out.println(res);
                    break;
                case 4:
                    JOptionPane.showMessageDialog(null, "Gracias por utilizar nuestra app", "¡SALIDA!",
                            JOptionPane.INFORMATION_MESSAGE);
                    break OUT;
                default:
                    JOptionPane.showMessageDialog(null, "¡Lea las opciones del menu!", "¡OPCIÓN INVALIDA!", JOptionPane.WARNING_MESSAGE);
            }

        }
        Persona p1 = new Persona("Mayela", 207850066, "Femenino", 1.50, 70.0, "Castaño");
        System.out.println(p1);

    }
}

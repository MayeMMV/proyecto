/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenmayelamejia;

/**
 *
 * @author Mayela Mejía Vela
 */
public class Persona {
    private String nombre;
    private int cedula;
    private String genero;
    private double estatura;
    private double peso;
    private String cabelloColor;

    public Persona(String nombre, int cedula, String genero, double estatura, double peso, String cabelloColor) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.genero = genero;
        this.estatura = estatura;
        this.peso = peso;
        this.cabelloColor = cabelloColor;
    }    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public double getEstatura() {
        return estatura;
    }

    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getCabelloColor() {
        return cabelloColor;
    }

    public void setCabelloColor(String cabelloColor) {
        this.cabelloColor = cabelloColor;
    }

    @Override
    public String toString() {
        return "Persona{" + "Su nombre es= " + nombre + ", Su cedula es= " + cedula + ", Su genero es= " + genero + ",Su estatura es= " + estatura + ",Su peso es= " + peso + ",El Color del cabello es= " + cabelloColor + '}';
    }
    
    
    
}
